Tricky hacks needed to run Windows on Thinkpad X60 with Coreboot.
Needless to say, **I'm not responsible in case you ruin your device
with this**.

## ROM config

Select X60 as the main board and switch from using native GPU
initialization to a custom video BIOS. I recommend using Seabios as
the main payload with GRUB as a secondary one.

## Video BIOS

Can be found online [here]. If the link dies, you'll need to spend a
few hours trying to find it using the name of the chipset as the
keyword.

[here]: https://driver.ru/?file_cid=9145740034249fdfd5f67168522

## Flashing Coreboot

Use a 32 bit Alpine Linux live image with the `iomem=relaxed` boot
parameter (may require editing the image). Flashrom can be used for
actual flashing. [Libreboot docs] have an instruction that can be used
here too.

[Libreboot docs]: https://libreboot.org/docs/install/#run-flashrom-on-host-cpu

## Windows version

Windows 2000 doesn't run at all and hangs before the installer can
even start.

Windows XP works, but finding drivers is a big pain in the ass.

Windows Vista works fine out of the box.
