#!/usr/bin/env python3

import sys
import fontforge

def main():
    args = sys.argv[1:]

    if len(args) != 3:
        print("usage: merge-fonts.py <input-a> <input-b> <output>")

        sys.exit(1)

    reciever_filename = args[0]
    donor_filename = args[1]
    output_filename = args[2]

    reciever = fontforge.open(reciever_filename)
    donor = fontforge.open(donor_filename)

    even_glyphs = list(range(0x0000, 0x2e7f, 2))

    reciever.selection.select(("unicode",), *even_glyphs)

    reciever.clear()
    reciever.selection.none()

    reciever.mergeFonts(donor)
    reciever.generate(output_filename)

if __name__ == "__main__":
    main()
