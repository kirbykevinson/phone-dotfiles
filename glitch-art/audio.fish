#!/usr/bin/fish

function raw
    ffmpeg -y\
        -i $argv[1]\
        \
        -f s16le\
        -ar 48k\
        -ac 1\
        \
        -c:a pcm_s16le\
        \
        $argv[2]
end

function unraw
    ffmpeg -y\
        -f s16le\
        -ar 48k\
        -ac 1\
        \
        -i $argv[1]\
        \
        $argv[2]
end

set args $argv[2..]

switch $argv[1]
    case raw
        raw $args
    case unraw
        unraw $args
    case '*'
        echo 'error: no such command'
end
