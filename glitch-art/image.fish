#!/usr/bin/fish

function raw
    set size (string replace 'x' ':' $argv[1])

    set input $argv[2]
    set output $argv[3]

    ffmpeg -y\
        -i $input\
        \
        -vf scale=$size\
        \
        -f rawvideo \
        -pix_fmt yuv444p16le\
        \
        $output
end

function unraw
    set size $argv[1]

    set input $argv[2]
    set output $argv[3]

    ffmpeg -y\
        -s:v $size\
        \
        -f rawvideo\
        -pix_fmt yuv444p16le\
        \
        -i $input\
        $output
end

function mashup
    set size $argv[1]

    set files $argv[2..]
    set raws

    set output (echo $argv | md5sum | head -c 32).png

    for file in $files
        set raws $raws $file.data

        raw $size $file $file.data
    end

    ./xor.py $raws $output.data

    unraw $size $output.data $output

    rm $raws $output.data
end

function resize
    set filter $argv[1]
    set size $argv[2]

    set input $argv[3]
    set output $argv[4]

    convert $input\
        -colorspace RGB\
        \
        -filter $filter\
        -resize "$size!"\
        \
        -colorspace sRGB\
        $output
end

set args $argv[2..]

switch $argv[1]
    case raw
        raw $args
    case unraw
        unraw $args
    case mashup
        mashup $args
    case resize-soft
        resize Gaussian $args
    case resize-sharp
        resize Sinc $args
    case '*'
        echo 'error: no such command'
end
