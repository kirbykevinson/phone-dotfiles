#!/usr/bin/env python3

import sys

BUF_SIZE = 512

def main():
    if len(sys.argv) < 4:
        print("usage: xor.py <input-a> <input-b> [input-c ...] <output>")

        sys.exit(1)

    input_filenames = sys.argv[1:-1]
    output_filename = sys.argv[-1]

    input_files = [open(filename, "rb") for filename in input_filenames]
    output_file = open(output_filename, "wb")

    while True:
        max_buf_len = 0

        input_ints = []

        for file in input_files:
            buf = file.read(BUF_SIZE)

            max_buf_len = max(max_buf_len, len(buf))

            the_int = int.from_bytes(buf, sys.byteorder)

            input_ints.append(the_int)

        if max_buf_len == 0:
            break

        output_int = input_ints[0]

        for the_int in input_ints[1:]:
            output_int ^= the_int

        output_bytes = output_int.to_bytes(max_buf_len, sys.byteorder)

        output_file.write(output_bytes)

    for file in input_files:
        file.close()

    output_file.close()

if __name__ == "__main__":
    main()
