#!/usr/bin/env python3

import sys

def main():
    offset = 8

    if len(sys.argv) > 1:
        offset = int(sys.argv[1])

    for line in sys.stdin:
        print(shift_codepoints(line, offset))

def shift_codepoints(string, offset):
    result = ""

    for chara in string:
        result += chr(ord(chara) << offset)

    return result

if __name__ == "__main__":
    main()
