#!/usr/bin/env python3

import math, sys

from enum import StrEnum
from PIL import Image

Mode = StrEnum("Mode",
    ["gray", "a", "b", "red", "green", "blue", "yellow", "invert", "film"]
)

def main():
    if len(sys.argv) < 2:
        print('usage: oklab.py <gray/a/b/red/green/blue/yellow/invert> [file ...]')

        sys.exit(1)

    mode = Mode(sys.argv[1])

    transform = globals()[mode]

    for file in sys.argv[2:]:
        image = Image.open(file).convert("RGB")

        data = list(image.getdata())

        for i in range(len(data)):
            pixel = data[i]

            pixel = desaturate(pixel, transform)

            data[i] = pixel

        image.putdata(data)

        image.save(file)

# Values for a and b are derived from the values of their corresponding pure
# sRGB color.

def gray(color):
    return (color[0], 0, 0)

def a(color):
    return (color[0], color[1], 0)

def b(color):
    return (color[0], 0, color[2])

def red(color):
    return (
        color[0],
        color[1] if color[1] > 0 else 0,
        color[1] / 1.7868071106680694 if color[1] > 0 else 0
    )

def green(color):
    return (
        color[0],
        color[1] if color[1] < 0 else 0,
        color[1] / -1.3030058768323223 if color[1] < 0 else 0
    )

def blue(color):
    return (
        color[0],
        color[2] / 9.598185279893794 if color[2] < 0 else 0,
        color[2] if color[2] < 0 else 0
    )

def yellow(color):
    return (
        color[0],
        color[2] / -2.7822938677007385 if color[2] > 0 else 0,
        color[2] if color[2] > 0 else 0
    )

def invert(color):
    return (
        1 - color[0],
        color[1] * -1,
        color[2] * -1
    )

def film(color):
    L = color[0]
    a = color[1]
    b = color[2]

    a += 0.05 * L
    b += 0.05 * (1 - L)

    return (L, a, b)

def desaturate(pixel, transform):
    oklab = linear_srgb_to_oklab(transfer(pixel))

    oklab = transform(oklab)

    rgb = untransfer(oklab_to_linear_srgb(oklab))

    return rgb

def transfer(pixel):
    return (
        f_inv(pixel[0] / 255),
        f_inv(pixel[1] / 255),
        f_inv(pixel[2] / 255),
    )

def untransfer(pixel):
    return (
        int(f(pixel[0]) * 255),
        int(f(pixel[1]) * 255),
        int(f(pixel[2]) * 255),
    )

# Stolen from https://bottosson.github.io/posts/oklab/

def linear_srgb_to_oklab(c):
    l = 0.4122214708 * c[0] + 0.5363325363 * c[1] + 0.0514459929 * c[2]
    m = 0.2119034982 * c[0] + 0.6806995451 * c[1] + 0.1073969566 * c[2]
    s = 0.0883024619 * c[0] + 0.2817188376 * c[1] + 0.6299787005 * c[2]

    l_ = math.cbrt(l)
    m_ = math.cbrt(m)
    s_ = math.cbrt(s)

    return (
        0.2104542553 * l_ + 0.7936177850 * m_ - 0.0040720468 * s_,
        1.9779984951 * l_ - 2.4285922050 * m_ + 0.4505937099 * s_,
        0.0259040371 * l_ + 0.7827717662 * m_ - 0.8086757660 * s_,
    )

def oklab_to_linear_srgb(c):
    l_ = c[0] + 0.3963377774 * c[1] + 0.2158037573 * c[2]
    m_ = c[0] - 0.1055613458 * c[1] - 0.0638541728 * c[2]
    s_ = c[0] - 0.0894841775 * c[1] - 1.2914855480 * c[2]

    l = l_*l_*l_
    m = m_*m_*m_
    s = s_*s_*s_

    return (
        +4.0767416621 * l - 3.3077115913 * m + 0.2309699292 * s,
        -1.2684380046 * l + 2.6097574011 * m - 0.3413193965 * s,
        -0.0041960863 * l - 0.7034186147 * m + 1.7076147010 * s,
    )

# Stolen from https://bottosson.github.io/posts/colorwrong/#what-can-we-do%3F

def f(x):
    if x >= 0.0031308:
        return (1.055) * x**(1.0/2.4) - 0.055
    else:
        return 12.92 * x

def f_inv(x):
    if x >= 0.04045:
        return ((x + 0.055)/(1 + 0.055))**2.4
    else:
        return x / 12.92

if __name__ == "__main__":
    main()
