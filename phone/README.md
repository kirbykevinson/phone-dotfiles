Can't really backup Android configs, so here's my program list
instead.

## Local

* Grocery store, pharmacy, etc apps
* Yandex Maps

## F-Droid + IzzyOnDroid repo

* Aegis Authenticator
* Binary Eye
* Breezy Weather
* CalcYou
* Droid-ify
* Feeder
* Fossify Contacts
* Fossify Phone
* Fossify SMS Messenger
* Fossify Voice Recorder
* Material Files
* MotionMate
* NewPipe
* VLC

## Official Google

* Calendar
* Clock
* Drive
* Gmail
* Keep
* Photos
* Play
* Tasks
* Translate

## Third party Google Play

* Bitwarden
* Discord
* Firefox
* HedgeCam 2
* Microsoft Outlook
* ONLYOFFICE Documents
* Shazam
* Telegram
* WhatsApp
* iNaturalist
