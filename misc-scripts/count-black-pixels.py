#!/usr/bin/env python3

import sys
from PIL import Image

pixel_count = 0
black_count = 0

for file in sys.argv[1:]:
    image = Image.open(file).convert("RGBA")
    pixel_map = image.load()

    for x in range(image.size[0]):
        for y in range(image.size[1]):
            pixel = pixel_map[x, y]

            pixel_count += 1

            if (
                pixel[0] == 0 and
                pixel[1] == 0 and
                pixel[2] == 0
            ):
                black_count += 1

print("{}%".format(black_count / pixel_count * 100))
