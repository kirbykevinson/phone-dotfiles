#!/usr/bin/fish

ffmpeg -y\
    -loop 1\
    \
    -i $argv[1]\
    -i $argv[2]\
    \
    -shortest\
    -vf format=yuv420p\
    \
    -b:a 256k\
    -tune stillimage\
    \
    $argv[3]
