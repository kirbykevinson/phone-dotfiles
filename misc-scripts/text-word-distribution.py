#!/usr/bin/env python3

import sys, re

words = {}

for filename in sys.argv[1:]:
    with open(filename, "r") as file:
        text = file.read().lower()
        word_list = re.split(r"\W", text)

        for word in word_list:
            if word in words:
                words[word] += 1
            else:
                words[word] = 1

del words[""]

word_occurrencies = sorted(words.items(), key=lambda x: x[1])
word_occurrencies.reverse()

for word in word_occurrencies:
    print(word[1], word[0])
