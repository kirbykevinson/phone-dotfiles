#!/usr/bin/env python3

import sys

def parse_point(numbers):
    x = float(numbers[0])
    y = float(numbers[1])

    return (x, y)

def pathify(string):
    path = []

    pairs = string.strip().split(" ")
    numbers = [pair.split(",") for pair in pairs]
    points = [parse_point(number) for number in numbers]

    for i, point in enumerate(points):
        x = point[0]
        y = point[1]

        if i == 0:
            path.append(f"M {x:g} {y:g}")

            continue

        point0 = points[i - 1]

        x0 = point0[0]
        y0 = point0[1]

        dx = x - x0
        dy = y - y0

        path.append(f"l {dx:g} {dy:g}")

        if i == len(points) - 1:
            path.append("Z")

    return " ".join(path)

for line in sys.stdin:
    print(pathify(line))
