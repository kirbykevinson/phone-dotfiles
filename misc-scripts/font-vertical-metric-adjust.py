#!/usr/bin/env python3

import fontforge, sys

if len(sys.argv) != 3:
    print(
        "usage: font-vertical-metric-adjust.py [file] [amount]",
        file=sys.stderr
    )

    sys.exit(1)

filename = sys.argv[1]
amount = int(sys.argv[2])

font = fontforge.open(filename)

font.hhea_ascent -= amount
font.hhea_descent -= amount

font.os2_winascent -= amount
font.os2_windescent += amount

font.generate(filename)
