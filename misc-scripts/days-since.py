#!/usr/bin/env python3

import sys
from datetime import date

today = date.today()
start = date(2000, 1, 1)

argc = len(sys.argv)

if argc > 2:
    print("usage: ,since [date]", file=sys.stderr)

    sys.exit(1)

if argc > 1:
    start = date.fromisoformat(sys.argv[1])

delta = today - start
days = delta.days

qw = days / 28
ytm = qw / 13

print(f"""Today: {today.isoformat()}
Start: {start.isoformat()}

It's been {days} days
= {qw:.3f} quadroweeks
= {ytm:.3f} year™s
Today is day {days + 1}""")
