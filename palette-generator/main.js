#!/usr/bin/env node

import {light, dark} from "./colors.js";
import {hex} from "./okhsl.js";

main();

function main() {
    console.log("Light:");
    printPalette(light);

    console.log("Dark:");
    printPalette(dark);
}

function printPalette(palette, pageColorEquivalent) {
    console.log(
`
--border-color: #${hex(palette.border)};
--active-border-color: #${hex(palette.activeBorder)};

--foreground-color: #${hex(palette.foreground)};
--background-color: #${hex(palette.background)};
--page-color: #${hex(palette.page)};

--background-highlight: linear-gradient(
    to bottom, #${hex(palette.bgHighlight)}, transparent 8px
);
--background-shadow: linear-gradient(
    to top, #${hex(palette.bgShadow)}, transparent 8px
);

--accent-color: #${hex(palette.accent)};
--input-color: #${hex(palette.input)};
--button-color: #${hex(palette.button)};
--active-button-color: #${hex(palette.activeButton)};

--card-shadow: 0 1px 2px #${hex(palette.cardShadow)};
`
    );
}
