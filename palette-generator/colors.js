import {okhsl} from "./okhsl.js";

export const light = makePalette({
    accent: [250, 75, 40],
    saturation: 12,

    cardShadow: 60,

    foreground: 0,
    background: 95,
    page: 85,

    bgHighlight: 100,
    bgShadow: 85,

    input: 100,

    button: 90,
    activeButton: 85,

    border: 65,
    activeBorder: 40,
});

export const dark = makePalette({
    accent: [250, 75, 70],
    saturation: 12,

    cardShadow: 35,

    foreground: 100,
    background: 15,
    page: 5,

    bgHighlight: 20,
    bgShadow: 5,

    input: 10,

    button: 20,
    activeButton: 25,

    border: 45,
    activeBorder: 70,
});

function makePalette(parameters) {
    const accent = okhsl.apply(null, parameters.accent);

    const base = setSaturation(accent, parameters.saturation);

    return {
        accent,

        cardShadow: setLightness(base, parameters.cardShadow),

        foreground: setLightness(base, parameters.foreground),
        background: setLightness(base, parameters.background),
        page: setLightness(base, parameters.page),

        bgHighlight: setLightness(base, parameters.bgHighlight),
        bgShadow: setLightness(base, parameters.bgShadow),

        input: setLightness(base, parameters.input),

        button: setLightness(base, parameters.button),
        activeButton: setLightness(base, parameters.activeButton),

        border: setLightness(base, parameters.border),
        activeBorder: setLightness(base, parameters.activeBorder),
    };
}

function setSaturation(color, amount) {
    return color.with(1, amount / 100);
}

function setLightness(color, amount) {
    return color.with(2, amount / 100);
}
